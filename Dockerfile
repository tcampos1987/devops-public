# Use a imagem oficial do Nginx como base
FROM nginx:latest

# Copie o arquivo HTML para o diretório padrão de documentos do Nginx
COPY index.html /usr/share/nginx/html/index.html

# Exponha a porta 80 para o tráfego HTTP
EXPOSE 80

# Inicie o servidor Nginx
CMD ["nginx", "-g", "daemon off;"]

